package com.product.catalog.app.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.product.catalog.app.entity.Product;

public interface ProductRepository extends JpaRepository<Product, Long> {
    
}