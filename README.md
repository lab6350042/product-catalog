# API de Catálogo de Produtos

Esta é uma API de Catálogo de Produtos construída com Spring Boot em Java. 

Ela permite que você gerencie informações sobre produtos,
incluindo nome, descrição, preço, imagens e categorias. 

## Pré-requisitos

* Java 17
* Maven 3.8.7

## Instalação

1. Clone o repositório:

`git clone https://gitlab.com/lab6350042/product-catalog.git`


2. Acesse o diretório do projeto:

`cd product-catalog`


3. Instale as dependências:

`mvn install`


## Execução

Para executar o projeto, execute o seguinte comando:

`mvn spring-boot:run`


O projeto será executado na porta 8080.

## Contribições

Contribuições são bem-vindas. Para contribuir, faça um fork do repositório e envie um pull request.

## Licença

Este projeto está licenciado sob a licença MIT.
